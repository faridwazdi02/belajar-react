import React, { Component } from 'react';

class Child extends Component {
  render() {
    return (
    <div><p onClick={this.props.cfunc} >{this.props.inner}</p></div>
    );
  }
}

export default Child;
