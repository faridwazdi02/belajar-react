import React, { Component } from 'react';
import Child from './Child';

class Mine extends Component {
	
  constructor(){
  	super() ;
  	this.state = {texts:[], color: '', counter: 0} ;
  	this.changeColor = this.changeColor.bind(this); // fungsi yang dibuat sendiri haruss didaftarkan
  	this.counterClick = this.counterClick.bind(this); // fungsi yang dibuat sendiri haruss didaftarkan
  }

  componentDidMount (){
	  this.setState({texts: ['Hallo', 'Dunia', 'JS']}) ;
	  this.setState({color: 'green'}) ;
	  this.setState({counter: 0}) ;
  }

  changeColor(){
  	if(this.state.color === 'green')
  	{
  		this.setState({color: 'blue'});	
  	}else{
  		this.setState({color: 'green'});	
  	}
  }

  counterClick (){
  	this.setState({counter: this.state.counter + 1 });
  }

  render() {

  let reslt = this.state.texts.map((text) => 
  											 {
  											 	return <Child inner={text} cfunc={this.counterClick} /> ;
  											 	//Child = Import Child
  											 });
  let styles = {color:this.state.color, fontSize:'1.2rem'} ;

    return (
    <div style={styles} onClick={this.changeColor} >
    	<p>Hello World</p>
    	{reslt}
    	<p>{this.state.counter}</p>
    </div>
    );
  }
}

export default Mine;
